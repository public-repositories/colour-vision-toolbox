import ColourVisionToolbox as cv
using Test

λ_samp_range = 400.0:5.0:700.0
spd = rand(length(λ_samp_range),5)

λ_interpolation = 380.0:4.0:780.0

function test_spd2lms()
    lms = cv.SPD2LMS(
        spd,λ_samp_range;
        cone_fundamental_type = cv.StockmanSharpe,
        standard_observer_type = cv.deg2)

    return size(lms)
end


@testset "ColourSpaces.jl" begin
    # SPD2LMS proper output size
    @test test_spd2lms() == (3,size(spd,2))
end
