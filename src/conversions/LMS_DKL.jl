# M_bgLMS2DKL : NO EXPORT
function M_bgLMS2DKL(bgLMS::Vector{Float64})
    l,m,s = bgLMS

    # Non-normalised matrix based on constraints on the three channels
    M_dLMS2DKL_nonnormalised = [  1       1      0;
                                  1      -l/m    0;
                                 -1      -1      (l+m)/s ]

    # Normalise such that the unit DKL responoses are produced by stimuli
    # which also produce unit-pooled-contrast
    temp_ = inv(M_dLMS2DKL_nonnormalised .* transpose(bgLMS))
    M_dLMS2DKL = inv( bgLMS .* (temp_ ./ sqrt.(sum(temp_.^2, dims = 1))) )

    return M_dLMS2DKL
end

# LMS2DKL : export
function LMS2DKL(LMS::VecOrMat{Float64}, bgLMS::Vector{Float64})
    # dLMS
    dLMS = LMS2dLMS(LMS, bgLMS)

    # Get the unit-pooled matrix for dLMS -> DKL
    M_dLMS2DKL = M_bgLMS2DKL(bgLMS)

    # Convert all the dLMS values to DKL
    DKL = M_dLMS2DKL * dLMS

    return DKL
end

# DKL2LMS : export
function DKL2LMS(DKL::VecOrMat{Float64}, bgLMS::Vector{Float64})
    # Get the unit-pooled matrix for dLMS -> DKL
    M_dLMS2DKL = M_bgLMS2DKL(bgLMS)

    # Since we want the opposite transform, we just invert the above matrix
    dLMS = inv(M_dLMS2DKL) * DKL ;

    # Calculate the output parameter
    LMS = dLMS2LMS(dLMS, bgLMS)

    return LMS
end
