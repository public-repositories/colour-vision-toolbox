# LMS2bmL : export
function LMS2bmL(LMS::VecOrMat{Float64})
    Lum = sum( LMS[1:2,:], dims = 1)
    bmL = [ LMS[ [1,3],:] ./ Lum ; Lum]

    if isa(LMS,Vector)
        bmL = bmL[:]
    end

    return bmL
end

# bmL2LMS : export
function bmL2LMS(bmL::VecOrMat{Float64})
    LMS = [ bmL[1:1,:] ; (1.0 .- bmL[1:1,:]) ; bmL[2:2,:] ] .* bmL[3:3,:]

    if isa(bmL,Vector)
        LMS = LMS[:]
    end

    return LMS
end
