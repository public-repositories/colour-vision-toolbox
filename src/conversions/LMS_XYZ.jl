# Choose the right XYZ -> LMS matrix
function choose_M_XYZ2LMS(ref)
    """Transform from traditional CIE1931 tristimuli to Smith Pokorny LMS values.

The CIE2012 is based on the Stockman and Sharpe (2000) and 1931 CIE coordinates.
The Golz2003 method should work for most broadband 3-channel systems with roughly R, G and B primaries. 
The Wyszecki&Stiles method is the classical approach.
    """

    # Transform proposed in Golz 2003.
    # URL: https://opg.optica.org/josaa/abstract.cfm?uri=josaa-20-5-769
    if ref == "Golz2003"
        M_XYZ2LMS = [ 0.15282    0.54383   -0.02795;
                     -0.15254    0.45524    0.03355;
                     -0.00045    0.00145    0.95449 ]

    elseif ref == "StockmanSharpe2deg"
        M_XYZ2LMS = [ 0.1452935718  0.5899360553 -0.0273891491;
                     -0.1452851062  0.4100794906  0.0267720289;
                      0.0           0.0           0.0192041330 ]

    # Book: "Color Science: Concepts and Methods, Quantitative Data and Formulae"
    # Authors: Wyszecki & Stiles
    # URL: https://www.wiley.com/en-us/Color+Science%3A+Concepts+and+Methods%2C+Quantitative+Data+and+Formulae%2C+2nd+Edition-p-9780471399186
    elseif ref == "Wyszecki&Stiles"
        M_XYZ2LMS = [ 0.15514    0.54312    -0.03286;
                     -0.15514    0.45684     0.03286;
                      0.0	 0.0	     0.01608 ] 

    # CIE 2012 "physiologically relevant" XYZ to Stockman and Sharpe (2000)
    # Proposed during CIE 2016 meeting.
    # URL: http://www.cvrl.org/database/text/cienewxyz/cie2012xyz2.htm
    elseif ref == "CIE2012"
        M_XYZ2LMS = inv([ 1.94735469    -1.41445123    -0.36476327;
                          0.68990272     0.34832189              0;
                          1.93485343              0              0 ])


    end

    return M_XYZ2LMS
end

# LMS2XYZ : export
function LMS2XYZ(LMS::VecOrMat{Float64}; ref="Golz2003")

    M_XYZ2LMS = choose_M_XYZ2LMS(ref)
    XYZ = inv(M_XYZ2LMS) * LMS

    if isa(LMS,Vector)
        XYZ = XYZ[:]
    end

    return XYZ
end

# XYZ2LMS : export
function XYZ2LMS(XYZ::VecOrMat{Float64}; ref="Golz2003")

    M_XYZ2LMS = choose_M_XYZ2LMS(ref)
    LMS = M_XYZ2LMS * XYZ

    if isa(XYZ,Vector)
        LMS = LMS[:]
    end

    return LMS
end
