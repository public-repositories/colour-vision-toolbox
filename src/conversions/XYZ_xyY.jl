# XYZ2xyY : export
function XYZ2xyY(XYZ::VecOrMat{Float64})
    S = sum(XYZ,dims=1)
    Y = XYZ[2:2,:]

    x = XYZ[1:1,:] ./ S
    y = Y ./ S

    xyY = [x; y; Y]

    if isa(XYZ,Vector)
        xyY = xyY[:]
    end

    return xyY
end

# xyY2XYZ : export
function xyY2XYZ(xyY::VecOrMat{Float64})
    x = xyY[1:1,:]
    y = xyY[2:2,:]

    Y = xyY[3:3,:]
    X = x.*Y./y
    Z = (1 .- x .- y).*Y./y

    XYZ = [X; Y; Z]

    if isa(xyY,Vector)
        XYZ = XYZ[:]
    end

    return XYZ
end
