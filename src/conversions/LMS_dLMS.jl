# LMS2dLMS : export
function LMS2dLMS(LMS::VecOrMat{Float64}, bgLMS::Vector{Float64})
    return (LMS .- bgLMS)
end

# dLMS2LMS : export
function dLMS2LMS(dLMS::VecOrMat{Float64}, bgLMS::Vector{Float64})
    return (dLMS .+ bgLMS)
end
