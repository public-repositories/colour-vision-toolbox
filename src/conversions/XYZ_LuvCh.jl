# XYZ2CIELUV_nonlinearity_ : not exported
function XYZ2CIELUV_nonlinearity_(Y::VecOrMat{Float64}, Yn::Float64)
    # Normalising the argument (Ω) for the non-linearity using the reference
    Ω = Y ./ Yn

    # Calculating the non-linearity (f_omega)
    L = Ω
    ind_linearity = Ω .<= (6/29)^3
    ind_nonlinearity = .~ind_linearity

    @. L[ind_linearity]    = (29/3)^3 * Ω[ind_linearity]
    @. L[ind_nonlinearity] = 116.0 * Ω[ind_nonlinearity]^(1/3) - 16.0

    return L
end

# CIELUV2XYZ_nonlinearity_ : not exported
function CIELUV2XYZ_nonlinearity_(L::VecOrMat{Float64}, Yn::Float64)
    # Calculating the non-linearity (f_omega)
    Y = L
    ind_linearity = L .<= 8.0
    ind_nonlinearity = .~ind_linearity

    @. Y[ind_linearity]    = (3/29)^3 * Yn * L[ind_linearity]
    @. Y[ind_nonlinearity] = Yn * ((16.0 + L[ind_nonlinearity]) / 116.0)^3

    return Y
end

# XYZ2LUV : exported
function XYZ2Luv(XYZ::VecOrMat{Float64}, refXYZ::Vector{Float64})

    upvpY = XYZ2upvpY(XYZ)
    ref_upvpY = XYZ2upvpY(refXYZ)

    # Calculatet the non-linearity
    L = XYZ2CIELUV_nonlinearity_(XYZ[2:2,:],refXYZ[2])

    # Calculating u v
    u = 13* L .* (upvpY[1:1,:] .- ref_upvpY[1:1,:])
    v = 13* L .* (upvpY[2:2,:] .- ref_upvpY[2:2,:])

    Luv = [L; u; v]

    if isa(XYZ,Vector)
        Luv = Luv[:]
    end

    return Luv
end

# LUV2XYZ : exported
function Luv2XYZ(Luv::VecOrMat{Float64}, refXYZ::Vector{Float64})

    ref_upvpY = XYZ2upvpY(refXYZ)

    L = Luv[1:1,:]
    u = Luv[2:2,:]
    v = Luv[3:3,:]

    up = u ./ (13 .* L) .+ ref_upvpY[1:1,:]
    vp = v ./ (13 .* L) .+ ref_upvpY[2:2,:]

    # Calculatet the non-linearity
    Y = CIELUV2XYZ_nonlinearity_(L,refXYZ[2])

    # Calculating X and Z
    X = (9/4)* Y .* (up ./ vp)
    Z = Y .* (12.0 .- 3.0 .* up .- 20.0 .* vp) ./ (4.0 .* vp)

    XYZ = [X; Y; Z]

    if isa(Luv,Vector)
        XYZ = XYZ[:]
    end

    return XYZ
end

# XYZ2LuvCh : exported
function XYZ2LuvCh(XYZ::VecOrMat{Float64}, refXYZ::Vector{Float64})
    # Calculate Lab
    Luv = XYZ2Luv(XYZ,refXYZ)

    # Calculate C and h (in degrees)
    C = sqrt.(Luv[2:2,:].^2 .+ Luv[3:3,:].^2)
    h = atand.(Luv[3:3,:],Luv[2:2,:]) # ∈ [-π,π]

    LuvCh = [Luv; C; h]

    if isa(XYZ,Vector)
        LuvCh = LuvCh[:]
    end


    return LuvCh
end

# CIELUV_LCh2XYZ : exported
function CIELUV_LCh2XYZ(LCh::VecOrMat{Float64},refXYZ::Vector{Float64})

    L = LCh[1:1,:]
    C = LCh[2:2,:]
    h = LCh[3:3,:]

    u = C .* cosd.(h)
    v = C .* sind.(h)

    XYZ = Luv2XYZ([L;u;v],refXYZ)

    if isa(LCh,Vector)
        XYZ = XYZ[:]
    end

    return XYZ
end
