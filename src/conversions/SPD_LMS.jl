# SPD2LMS : export
function SPD2LMS(
    SPD::VecOrMat{Float64}, λ_sampling_range::StepRangeLen;
    λ_interpolation::Vector{Float64}=collect(380.0:4.0:780.0),
    cone_fundamental_type::ConeFundamentalType=StockmanSharpe,
    standard_observer_type::StandardObserverType=deg2
)
    "SPD: If matrix, columns correpond to wavelength bands."

    cf_data, cf_λ_range = read_cf_data(cone_fundamental_type,standard_observer_type)
    cf_data_interpolated = interpolate_SPD(cf_data, cf_λ_range, λ_interpolation)

    SPD_interpolated = interpolate_SPD(SPD, λ_sampling_range, λ_interpolation)

    LMS = transpose(cf_data_interpolated)*SPD_interpolated

    if isa(SPD,Vector)
        LMS = LMS[:]
    end


    return LMS
end
