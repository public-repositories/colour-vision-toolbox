# XYZ2upvpY : exported
function XYZ2upvpY(XYZ::VecOrMat{Float64})
    X = XYZ[1:1,:]
    Y = XYZ[2:2,:]
    Z = XYZ[3:3,:]

    up = 4*X ./ (X .+ 15*Y .+ 3*Z)
    vp = 9*Y ./ (X .+ 15*Y .+ 3*Z)

    upvpY = [up; vp; Y]

    if isa(XYZ,Vector)
        upvpY = upvpY[:]
    end

    return upvpY
end

# upvpY2XYZ : exported
function upvpY2XYZ(upvpY::VecOrMat{Float64})
    up = upvpY[1:1,:]
    vp = upvpY[2:2,:]
    Y  = upvpY[3:3,:]

    x = 9*up ./ (6*up .- 16*vp .+ 12.0)
    y = 4*vp ./ (6*up .- 16*vp .+ 12.0)

    xyY = [x; y; Y]
    XYZ = xyY2XYZ(xyY)

    if isa(upvpY,Vector)
        XYZ = XYZ[:]
    end

    return XYZ
end
