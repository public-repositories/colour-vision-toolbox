# XYZ2CIELAB_nonlinearity_ : not exported
function XYZ2CIELAB_nonlinearity_(XYZ::VecOrMat{Float64}, refXYZ::Vector{Float64})
    # Normalising the argument (Ω) for the non-linearity using the reference
    Ω = XYZ ./ refXYZ

    # Calculating the non-linearity (f_omega)
    ζ = Ω
    ind_linearity = Ω .<= (6/29)^3
    ind_nonlinearity = .~ind_linearity

    @. ζ[ind_linearity]    = (1/3) * (29/6)^2 * Ω[ind_linearity] + (4/29)
    @. ζ[ind_nonlinearity] = Ω[ind_nonlinearity]^(1/3)

    return ζ
end

# CIELAB2XYZ_nonlinearity_ : not exported
function CIELAB2XYZ_nonlinearity_(t::VecOrMat{Float64})

    # Calculating the non-linearity (f_omega)
    ind_linearity = t .<= (6/29)^3
    ind_nonlinearity = .~ind_linearity

    ζinv = t
    @. ζinv[ind_linearity]    = 3 * (6/29)^2 * (t[ind_linearity] - (4/29))
    @. ζinv[ind_nonlinearity] = t[ind_nonlinearity]^3

    return ζinv
end

# XYZ2Lab : exported
function XYZ2Lab(XYZ::VecOrMat{Float64}, refXYZ::Vector{Float64})
    # Calculatet the non-linearity
    ζ = XYZ2CIELAB_nonlinearity_(XYZ,refXYZ)

    # Calculating L a b C and h (in degrees)
    L = 116*ζ[2:2,:] .- 16

    a = 500*(ζ[1:1,:] .- ζ[2:2,:])
    b = 200*(ζ[2:2,:] .- ζ[3:3,:])

    Lab = [L; a; b]

    if isa(XYZ,Vector)
        Lab = Lab[:]
    end

    return Lab
end

# Lab2XYZ : exported
function Lab2XYZ(Lab::VecOrMat{Float64},refXYZ::Vector{Float64})
    Xn,Yn,Zn = refXYZ

    L = Lab[1:1,:]
    a = Lab[2:2,:]
    b = Lab[3:3,:]

    t_Y = (L .+ 16)/116
    t_X = t_Y .+ (a/500)
    t_Z = t_Y .- (b/200)

    X = Xn .* CIELAB2XYZ_nonlinearity_(t_X)
    Y = Yn .* CIELAB2XYZ_nonlinearity_(t_Y)
    Z = Zn .* CIELAB2XYZ_nonlinearity_(t_Z)

    XYZ = [X; Y; Z]

    if isa(Lab,Vector)
        XYZ = XYZ[:]
    end

    return XYZ
end

# XYZ2LabCh : exported
function XYZ2LabCh(XYZ::VecOrMat{Float64}, refXYZ::Vector{Float64})
    # Calculate Lab
    Lab = XYZ2Lab(XYZ,refXYZ)

    # Calculate C and h (in degrees)
    C = sqrt.(Lab[2:2,:].^2 .+ Lab[3:3,:].^2)
    h = atand.(Lab[3:3,:],Lab[2:2,:]) # ∈ [-π,π]

    LabCh = [Lab; C; h]

    if isa(XYZ,Vector)
        LabCh = LabCh[:]
    end

    return LabCh
end

# CIELAB_LCh2XYZ : exported
function CIELAB_LCh2XYZ(LCh::VecOrMat{Float64},refXYZ::Vector{Float64})

    L = LCh[1:1,:]
    C = LCh[2:2,:]
    h = LCh[3:3,:]

    a = C .* cosd.(h)
    b = C .* sind.(h)

    XYZ = Lab2XYZ([L;a;b],refXYZ)

    if isa(LCh,Vector)
        XYZ = XYZ[:]
    end

    return XYZ
end
