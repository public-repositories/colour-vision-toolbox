# Hs2ΔH : export
function Hs2ΔH(h1::Vector{Float64}, h2::Vector{Float64})
    """All angles are in degress from the positive a axis."""

    eh₁ = exp.(im*deg2rad.(h1))
    eh₂ = exp.(im*deg2rad.(h2))

    Δh = abs.(rad2deg.(angle.( eh₂ ./ eh₁ ))) # ∈ [-π,π]

    return Δh[:]
end
