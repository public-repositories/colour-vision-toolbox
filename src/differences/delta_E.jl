# Labs2ΔE : export
function Labs2ΔE(Lab1::VecOrMat{Float64}, Lab2::VecOrMat{Float64})
    ΔE = sqrt.( sum( (Lab1 .- Lab2).^2, dims = 1) )

    return ΔE[:]
end

