module ColourVisionToolbox

# External modules
using LinearAlgebra, Interpolations, JLD2, FileIO

# Exports
# using Base: active_project
export
    LMS2dLMS, dLMS2LMS,
    LMS2bmL, bmL2LMS,
    LMS2DKL, DKL2LMS,
    LMS2XYZ, XYZ2LMS,
    XYZ2xyY, xyY2XYZ,
    XYZ2upvpY, upvpY2XYZ,
    XYZ2Lab, Lab2XYZ,
    XYZ2LabCh, CIELAB_LCh2XYZ,
    XYZ2Luv, Luv2XYZ,
    XYZ2LuvCh, CIELUV_LCh2XYZ,
    Labs2ΔE, Hs2ΔH,
    ConeFundamentalType, StandardObserverType,
    interpolate_SPD, SPD2LMS,
    read_cf_data


# Directories
# PROJECT_DIR = @__MODULE__
PROJECT_DIR = normpath(joinpath(@__DIR__,".."))
DATA_DIR = joinpath(PROJECT_DIR,"data")

# datatypes
include("./utils/datatypes.jl")
# functions
include("./utils/functions.jl")

# Daylight locus
include("./utils/daylight.jl")

# LMS <--> dLMS
include("./conversions/LMS_dLMS.jl")

# LMS <--> bmL
include("./conversions/LMS_bmL.jl")

# LMS <--> DKL
include("./conversions/LMS_DKL.jl")

# LMS <--> XYZ
include("./conversions/LMS_XYZ.jl")

# XYZ <--> xyY
include("./conversions/XYZ_xyY.jl")

# XYZ <--> CIELAB
include("./conversions/XYZ_LabCh.jl")

# XYZ <--> CIELuv
include("./conversions/XYZ_LuvCh.jl")

# XYZ <--> upvpY
include("./conversions/XYZ_upvpY.jl")

# SPD --> LMS
include("./conversions/SPD_LMS.jl")

# H x H --> ΔH (H is a planar angle)
include("./differences/delta_H.jl")

# Lab x Lab --> ΔH (H is a planar angle)
include("./differences/delta_E.jl")

end # Module
