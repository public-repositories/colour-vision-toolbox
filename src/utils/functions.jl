# Using cubic splines (each column is a spectrum)
function interpolate_SPD(
    SPD::VecOrMat{Float64},
    λ_sampling::StepRangeLen,
    λ_interpolation::Vector{Float64}
)
    msg = "SPD and λ_sampling length mismatch."
    @assert size(SPD)[1] == length(λ_sampling) msg

    SPD_interpolated = cat([
        CubicSplineInterpolation(
            λ_sampling, SPD_col;
            extrapolation_bc = Line()
        )(λ_interpolation)
        for SPD_col in eachcol(SPD)]...; dims=2)

    return SPD_interpolated
end

function interpolate_SPD(
    SPD::VecOrMat{Float64},
    λ_sampling::Vector{Float64},
    λ_interpolation::Vector{Float64}
)
    msg = "SPD and λ_sampling length mismatch."
    @assert size(SPD)[1] == length(λ_sampling) msg

    return interpolate_SPD(
        SPD,
        λ_sampling[1]:λ_sampling[2]-λ_sampling[1]:λ_sampling[end],
        λ_interpolation
    )
end


# Reading the fundamentals : exported
function read_cf_data(
    cone_fundamental_type::ConeFundamentalType,
    standard_observer_type::StandardObserverType
)

    if cone_fundamental_type == StockmanSharpe
        fundamental_type = "StockmanSharpe"
    elseif cone_fundamental_type == SmithPokorny
        fundamental_type = "SmithPokorny"
    end

    if standard_observer_type == deg2
        observer_type = "2deg"
    elseif standard_observer_type == deg10
        observer_type = "10deg"
    end

    file_name = fundamental_type*"_"*observer_type*"Obs_390_1_830.jld2"
    file_path = joinpath(DATA_DIR,file_name)

    raw_cf_data = load(file_path)["cf_2deg_390_1_830"]

    λ = raw_cf_data[:,1]
    # λ_range = λ[1]:(λ[2] - λ[1]):λ[end]

    cone_fundamentals = raw_cf_data[:,2:4]

    if cone_fundamental_type == StockmanSharpe
        cone_fundamentals = cone_fundamentals * diagm(
            [0.689903, 0.348322, 0.0371597]
        )
    end

    # return cone_fundamentals, λ_range
    return cone_fundamentals, λ
end
