# Daylight locus
# NOTE: This does NOT give the associated colour temperatures
function generate_daylight_xy(;N=50,x_start=0.2,x_stop=0.5)
    x = range(start=x_start,stop=x_stop,length = N)
    y = -3.000*x.^2 .+ 2.870*x .- 0.275

    return collect([x y]')
end
