# Colour Vision Toolbox

Colour Vision Toolbox is a small toolbox which does basic colourspace manipulations. It has been developed for use in vision science pipelines, and implements minimal (almost no) checks for data corruption. It assumes that the colour data is stored as a regular array (1-D or 2-D), and implements no special types.

If you use this toolbox in your projects, please cite this repository (or one of my papers which uses this toolbox).


# Examples

The conversion of CIE 1931 XYZ coordinates to CIELAB coordinates can be done by:

```julia
import ColourVisionToolbox as cv
xyz = rand(3,100)
white_point = rand(3)

lab = cv.XYZ2Lab(xyz,white_point)
```

To convert these values to cone-excitations using the linear map estimated in [Golz and MacLeod, 2003](https://doi.org/10.1364/JOSAA.20.000769):

```julia
lms = cv.XYZ2LMS(xyz; ref="Golz2003")
```

If you need the colours in [DKL](https://doi.org/10.1113/jphysiol.1984.sp015499) space:

```julia
bg_LMS = cv.XYZ2LMS(white_point;ref="Golz2003")
dkl = cv.LMS2DKL(lms,bg_LMS)
```


# Contributions

Contributions will be accepted in the following cases:

- Errors!

- Implementations of algorithms for channel-wise linearisation of displays.

- A faster/cleverer implementation of a computation already included in the toolbox.

- Industry-focussed computations such as the CIECAM02 and various colour difference metrics.

- More numerically stable and/or accurate implementations of spectral interpolation. If the interpolation uses a specific dataset, you must be willing to share it under a permissive license.

- Models for lens and macular filtering, photoreceptor sampling, and other similar pre-cortical filters.
